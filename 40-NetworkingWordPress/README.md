# Übung: Drei WordPress Container mit Netz und Mounts

Wir komplettieren die Container-Techniken und verdrahten unsere Container mit

*   Netzwerken - `docker network create ...`
*   Mounts - Ordnerbindungen zwischen Container und (lokalem) Dockerhost

![Docker und Container](../99-DATA/drei-container-planen-800px.jpg "Docker Netzwerk für WordPress Container")

**Hinweis:** Bitte mit echtem Docker Host (unter Linux) testen! 
Dann funktionieren auch die EOL (Backslash) für *Copy & Paste*.


## Netzwerk für WordPress Container:

Wir erstellen uns ein neues Docker Netzwerk:

    docker network create test-net

Anm.: wenn nicht angegeben `--driver bridge`

Übersicht über die Docker Netzwerke:

    docker network ls

Und bitte immer mal wieder `docker inspect ...`.

## DB-Container: MariaDB4WP (Datenbank)

    docker run -d --name MariaDB4WP \
        --network test-net \
        --env MARIADB_USER=wpuser \
        --env MARIADB_PASSWORD=geheim \
        --env MARIADB_ROOT_PASSWORD=geheim \
        --env MARIADB_DATABASE=wpexample \
        -v /home/joeb/dockerdata/wpmysql:/var/lib/mysql \
        mariadb:latest

## WordPress-Container: WP01 (Webserver)

    docker run -d --name WP01 \
        --network test-net -p 8081:80 \
        -e WORDPRESS_DB_PASSWORD=geheim \
        -e WORDPRESS_DB_HOST=MariaDB4WP \
        -e WORDPRESS_DB_USER=wpuser \
        -e WORDPRESS_DB_NAME=wpexample \
        -v /home/joeb/dockerdata/wphtml:/var/www/html \ 
        wordpress

## PhpMyAdmin Container: PMA01

    docker run -d --name PMA01 \
        --network test-net -p 8080:80 \
        -e PMA_HOST=MariaDB4WP \
        phpmyadmin/phpmyadmin

Und die Tests - wie immer - mittels Browser im Docker-Host.

Dieses Beispiel soll zeigen, dass die gemeinsame Nutzung mehrerer Container viele Anforderungen und Tipparbeit kostet. Mit Hilfe weiterführender Techniken (siehe Docker Compose) wollen wir uns diese Arbeit erleichtern.