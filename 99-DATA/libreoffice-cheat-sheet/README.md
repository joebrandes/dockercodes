# Cheat Sheet für Docker mit LibreOffice Draw

Ein Klassiker aus den Anfangszeiten meiner Docker/Container Seminare.

![Docker Cheat Sheet Joe B.](./docker-cheat-sheet-libreoffice-draw.jpg "Docker Cheat Sheet Joe B.")

Ich lege die letzte `odg` Datei für **LibreOffice Draw** hier mit bei.

[Aktualisierung Feb 2024]