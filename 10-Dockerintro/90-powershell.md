# PowerShell

Da wir unseren Einstieg in die "Docker/Container Welt" in einer Windows OS-Umgebung durchführen, sollten wir uns mit der PowerShell als Konsole anfreunden.

Für einen schnellen Einstieg in die effektive Nutzung der PowerShell Konsole stelle ich ein Gitlab-Repo mit einem guten Start-Profile zur Verfügung.

## PowerShell Umgebung

Die Anleitungen werden im Seminar entsprechend kommentiert und erläutert.
Hier die Kurzanleitung: (wir benötigen die Software Git  - z.B. choco install git )

*   PowerShell öffnen - ExecutionPolicy setzen:

        Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser 

*   Gitlab Repo joebrandes für das WindowPowerShell Profil klonen:

        cd ~/Documents 
        git clone https://gitlab.com/joebrandes/WindowsPowerShell.git 

*   Font (MesloLGS NF / FiraCode) installieren (Font in Repo)

*   Starship installieren 
    
        winget install --id Starship.Starship #  oder natürlich mit
        choco install starship

*   Starship aktivieren - in $PROFILE-Datei auskommentieren (siehe Dateiende) 
und Konfiguration kopieren

Tipp: Alle Konsolentools der Woche müssen einfach nur in Profilordner 
`~/Documents/WindowsPowerShell/_jbtools` kopiert werden!

## DockerCompletion (PowerShell Modul)

Für die Docker Code-Vervollständigungen installieren wir einfach noch 
das Modul DockerCompletion:

    # Install from PowerShell Gallery
    Install-Module DockerCompletion -Scope CurrentUser
    # Import (bitte einfach in $PROFILE eintragen)
    Import-Module DockerCompletion

Github Repo für das Modul DockerCompletion: https://github.com/matt9ucci/DockerCompletion