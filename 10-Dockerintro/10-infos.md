# Unterlagen für TN Dockerseminare Joe Brandes

![Docker und Container](../99-DATA/timelab-pro-sWOvgOOFk1g-unsplash.jpg "Docker und Container")

RST-Unterlage Online (HTML, Singlehtml, PDF)

*   [Seminar Docker Trainer Joe Brandes](http://docker.joe-brandes.de/ "Seminarunterlage")

Seminarbeiträge auf Website: 

*   [PC Systembetreuer Portal](http://pcsystembetreuer.de)

*   [Fachkraft IT-Systeme und -Netzwerke](http://fitsn.de)


Dieses Gitlab Repo

*   [Dockercodes auf Joe Brandes Gitlab Repo](https://gitlab.com/joebrandes/dockercodes.git "Dockercodes")

Beispiele für Docker / Docker Compose

*   [Dockerbuch Öggl und Kofler / Rheinwerk Verlag](https://github.com/docbuc "Dockerbuch")
*   [Docker Github Repo Awesome Compose](https://github.com/docker/awesome-compose "Docker Awesome Compose")
