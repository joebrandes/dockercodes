# Erste Docker Befehle

In lockerer Schüttung:

    docker version

    docker info | grep -i registry        # Linux
    docker info | Select-String registry  # Windows 

    docker run hello-world
    docker ps -a  # klassischer Aufruf
    docker container ls -a   # Aufruf seit 1.12 / 1.13
    
    docker images  # klassischer Aufruf
    docker image ls  # wieder der modernere / stringentere Aufruf

    docker help
    docker container ls --help

    docker run -it --name meinubuntu --hostname meinubu ubuntu:18.04 
    docker start -i meinubuntu 

    docker container rm <container-name> 

    docker image rm <image-name>
    docker rmi 

    # apt install docker-bash-completion 

    docker container ls -a # oder auch kürzer und klassisch mit
    docker ps -a



To bo continued...

Bitte Beiträge / Ausarbeitungen besuchen.