# WordPress mit Docker Compose

Statt manuelle Container zu bauen, lassen wir jetzt 
`docker compose ...` die Arbeit erledigen.

Wir konzentrieren uns auf den Web- und Datenbankservice
mit der beigelegten `docker-compose.yml` Datei (siehe Öggl/Kofler docbuc):

[Quelle: Github Repo Dockerbuch Öggl / Kofler](https://github.com/docbuc/docker-compose/blob/e800ae7db62aa1192821948f3bf279b34cd10297/wordpress-beispiel/docker-compose.yml)

